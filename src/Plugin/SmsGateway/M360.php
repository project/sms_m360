<?php

namespace Drupal\sms_m360\Plugin\SmsGateway;

use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResultStatus;
use GuzzleHttp\Client;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SmsGateway(
 *   id = "'sms_m360'",
 *   label = @Translation("SMS M360"),
 *   outgoing_message_max_recipients = 1,
 *   reports_push = TRUE,
 *   incoming = TRUE,
 *   incoming_route = TRUE
 * )
 */
class M360 extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  private const REPORT_ERRORS = [
    1 => SmsMessageReportStatus::INVALID_RECIPIENT,
    6 => SmsMessageReportStatus::CONTENT_INVALID,
  ];

  private const RESULT_ERRORS = [
    401 => SmsMessageResultStatus::ACCOUNT_ERROR,
    400 => SmsMessageResultStatus::PARAMETERS,
  ];

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $client;
    $this->setLoggerFactory($logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): M360 {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'username' => '',
      'password' => '',
      'sender_id' => '',
      'local_calling_codes' => '63',
      'url' => 'https://api.m360.com.ph/v3/api/broadcast',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['m360'] = [
      '#type' => 'details',
      '#title' => $this->t('M360'),
      '#open' => TRUE,
    ];

    $form['m360']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('API can be found at <a href="@url">@url</a>.', [
        '@url' => 'https://m360.com.ph/api-docs',
      ]),
    ];

    $form['m360']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $config['url'],
      '#required' => TRUE,
    ];

    $form['m360']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config['username'],
      '#placeholder' => 'XXXXXX',
      '#required' => TRUE,
    ];

    $form['m360']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config['password'],
      '#required' => TRUE,
    ];

    $form['m360']['sender_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender Id'),
      '#default_value' => $config['sender_id'],
      '#required' => TRUE,
    ];

    $form['m360']['local_calling_codes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Local Calling Codes'),
      '#default_value' => $config['local_calling_codes'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['username'] = trim($form_state->getValue('username'));
    $this->configuration['password'] = trim($form_state->getValue('password'));
    $this->configuration['sender_id'] = trim($form_state->getValue('sender_id'));
    $this->configuration['local_calling_codes'] = trim($form_state->getValue('local_calling_codes'));
    $this->configuration['url'] = trim($form_state->getValue('url'));
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message): SmsMessageResult {
    $config = $this->getConfiguration();
    $recipient = $sms_message->getRecipients()[0];
    $message = $sms_message->getMessage();

    $message_result = new SmsMessageResult();
    $delivery_report = new SmsDeliveryReport();

    $delivery_report->setRecipient($recipient);
    $delivery_report->setStatus(SmsMessageReportStatus::QUEUED);
    $delivery_report->setMessageId($sms_message->getUuid());

    try {
      // Hits M360 API to send Message.
      $request_json = [
        'username' => $config['username'],
        'password' => $config['password'],
        'msisdn' => $recipient,
        'content' => $message,
        'shortcode_mask' => $config['sender_id'],
        'is_intl' => self::IsInternationalPhoneNumber($recipient, $config['local_calling_codes']),
      ];
      $response = $this->httpClient->request('POST', $config['url'], [
        'json' => $request_json,
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ]);

      // Gets response from M360.
      $response_content = $response->getBody()->getContents();
      $data = json_decode($response_content, TRUE);

      \Drupal::logger('sms_m360')->debug('REQUEST: @request - RESPONSE: @response', [
        '@request' => json_encode($request_json, JSON_PRETTY_PRINT),
        '@response' => json_encode($response_content, JSON_PRETTY_PRINT),
      ]);

      $code = isset($data['code']) ? $data['code'] : FALSE;
      if ($data['code'] == '201') {
        $delivery_report->setStatus(SmsMessageReportStatus::DELIVERED);
        $delivery_report->setStatusMessage($this->t('Message sent to: @recipient', ['@recipient' => $recipient]));
        $message_result->addReport($delivery_report);
        \Drupal::logger('sms_m360')->info('Message sent to: @recipient', ['@recipient' => $recipient]);
        return $message_result;
      }

      $report_status = !in_array($code, self::RESULT_ERRORS) ? SmsMessageReportStatus::ERROR : self::RESULT_ERRORS[$code];
      $result_status = !in_array($code, self::REPORT_ERRORS) ? SmsMessageResultStatus::ERROR : self::REPORT_ERRORS[$code];

      $delivery_report->setStatus($report_status);
      $delivery_report->setStatusMessage($delivery_report);

      $this->setLogMessage($delivery_report, $message);
    }
    catch (GuzzleException $exception) {
      $delivery_report->setStatus(SmsMessageReportStatus::ERROR);
      $delivery_report->setStatusMessage(SmsMessageReportStatus::ERROR);

      $result_status = SmsMessageResultStatus::ERROR;

      $this->setLogMessage($delivery_report, $message, $exception->getMessage());
    }

    $message_result->addReport($delivery_report);
    $message_result->setError($result_status);

    return $message_result;
  }

  /**
   * Sets log message if error occured.
   *
   * @param \Drupal\sms\Message\SmsDeliveryReport $report
   *   The report.
   * @param string $content
   *   The content.
   * @param string $error_message
   *   The error message.
   */
  protected function setLogMessage(SmsDeliveryReport $report, $content, $error_message = ''): void {
    $this->getLogger('sms_m360')
      ->error($this->t('There is a problem when sending message: @message to @number, because of @error_message.', [
        '@message' => $content,
        '@number' => $report->getRecipient(),
        '@error_message' => $error_message,
      ]));
  }

  /**
   * Detect Local Phone Number.
   * +63: Philippines.
   *
   * @param string $phone
   *   Phone number.
   * @param string $local_calling_codes
   *   Local calling codes
   *
   * @return string
   *   Prepared phone number.
   */
  public static function IsInternationalPhoneNumber($phone, $local_calling_codes = '63') {
    if (self::startsWith($phone, "+$local_calling_codes") || self::startsWith($phone, $local_calling_codes)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Start with.
   *
   * @param string $haystack
   *   String to search in.
   * @param string $needle
   *   String need.
   *
   * @return bool
   *   Result.
   */
  public static function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }

}
